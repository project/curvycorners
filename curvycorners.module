<?php
   /*============================================================================*\  
   ||                     -= curvyCorners Drupal module =-                       ||
   ||                                                                            ||
   || By Jordan Starcher                                                         ||
   || curvyCorners.module                                                        ||
   || Project Version 5.x-1.0                                                    ||
   || June 25, 2007                                                              ||
   || Released under the GNU license                                             ||
   ||                                                                            ||
   ||                              Copyright ©2007                               ||
   ||                       http://www.TheOverclocked.com                        ||
   \*============================================================================*/

/******************************************************************************
 * Drupal Hooks                                                               *
 ******************************************************************************/

/**
 * Implementation of hook_perm().
 */
function curvyCorners_perm() {
  return array('administer curvyCorners');
}

/**
 * Implementation of hook_menu().
 */
function curvyCorners_menu() {
  $items = array();
  $items[] = array(
    'path' => 'admin/settings/curvyCorners',
    'title' => t('curvyCorners'),
    'callback' => 'drupal_get_form',
    'callback arguments' => 'curvyCorners_settings',
    'access' => user_access('administer curvyCorners'),
    'type' => MENU_NORMAL_ITEM,
   );
  return $items;
}

/**
 * Implementation of hook_footer().
 */
function curvyCorners_footer($main = 0) {
  $path = curvyCorners_status();
  if ($path !== FALSE) {
    drupal_add_js($path);
    drupal_add_js(variable_get('curvyCorners_all_pages', NULL), 'inline');     
  }
  return '';
}

/******************************************************************************
 * Menu Callbacks                                                             *
 ******************************************************************************/

/**
 * The one and only curvyCorners configuration page
 */
function curvyCorners_settings() {
  $form['curvyCorners_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('curvyCorners status'),
    '#weight' => -10,
    '#description' => (curvyCorners_status() !== false) ? t('The curvyCorners javascript file is available.') : t('The curvyCorners javascript file is unavailable. Download it from <a href="http://www.curvycorners.net/download.php?file=curvyCorners-v1-2-9-beta.zip">http://www.curvycorners.net/download.php?file=curvyCorners-v1-2-9-beta.zip</a> and put inside your %curvyCorners_folder folder.', array('%curvyCorners_folder' => drupal_get_path('module', 'curvyCorners'))),
    );
   $form['curvyCorners_lite'] = array(
      '#type' => 'radios',
      '#title' => t('Select full version or lite version'),
      '#default_value' => variable_get('curvyCorners_lite', 0),
      '#options' => array(0 => t('Full'), 1 => t('Lite')),
      '#description' => t('Honestly I am not sure if you loose functionality using the lite version or not. I would recommend running the lite version unless you run into limitations.'),
    );
    $form['curvyCorners_all_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('curvyCorners to apply on all pages'),
      '#rows' => variable_get('curvyCorners_textarea', 10),
      '#default_value' => variable_get('curvyCorners_all_pages', ''),
      '#description' => t('Add curvyCorners on all pages on all themes. Documentation is available at <a href="http://www.curvycorners.net/usage.php">http://www.curvycorners.net/usage.php</a>. Enter everything that will go inside the script tags.'),
    );
    $form['curvyCorners_textarea'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => t('Set the default size for the above textbox'),
      '#default_value' => variable_get('curvyCorners_textarea', 10),
    );
  
  return system_settings_form($form);
}

/******************************************************************************
 * Module Functions                                                           *
 ******************************************************************************/

/**
 * Checks to see if the JavaScript files exist in the curvyCorners module folder
 */
function curvyCorners_status() {
  if (variable_get('curvyCorners_lite', 0) == 0) {
    $path = drupal_get_path('module', 'curvyCorners') .'/rounded_corners.inc.js'; 
  }else{
    $path = drupal_get_path('module', 'curvyCorners') .'/rounded_corners_lite.inc.js';
  }

  if (!is_file($path)) {
    return FALSE;
  }
  return $path;
}
